#include<iostream>
#include<cstdlib>

using namespace std;

class List{
	private:
		typedef struct node{
			string title;
			string singer;
			node* next;
		}* nodePtr;
		nodePtr head;
		nodePtr tail;
		nodePtr temp;
		nodePtr curr;
	public:
		List();
		void EnQueue(string addTitle, string addSinger);
		void DeQueue();
		void PrintQueue();
};

List::List()
{
	head = NULL;
	tail = NULL;
	temp = NULL;
	curr = NULL;
}

void List::EnQueue(string addTitle, string addSinger)
{
	nodePtr n = new node;
	n->title = addTitle;
	n->singer = addSinger;
	n->next = NULL;
	if(head!=NULL){
		curr=head;
		while (curr->next!=NULL){
			curr=curr->next;
		}
		curr->next = n;
		tail = n;
	}
	else{
		head = n;
		tail = n;
	}
	
}

void List::DeQueue()
{
	if(head==NULL){
		cout<<"Queue is empty!\n";
	}
	else if(head->next == NULL){
		head = NULL;
		cout<<"Queue is now empty!\n";
	}
	else{
		curr = head;
		head = head->next;
		curr->next = NULL;
		cout<<"First item has been unqueued!\n";
	}
}

void List::PrintQueue()
{
	if (head==NULL){
		cout<<"Nothing to display!\n";
	}
	else{
		curr=head;
		while(curr!=NULL){
			cout<<"Title: "<<curr->title<<" by: "<<curr->singer<<endl;
			curr=curr->next;
		}
	}
}

List list;

void switcher()
{
	cout<<"1. EnQueue 2. DeQueue 3. Display Queue\n";
	cout<<"Enter choice: ";
	char c;
	cin>>c;
	cin.ignore(1,'\n');
	switch(c){
		case '1':{
			string addTitle, addSinger;
			cout<<"Enter title: ";
			getline(cin,addTitle);
			cout<<"Enter singer: ";
			getline(cin,addSinger);
			list.EnQueue(addTitle,addSinger);
			cout<<"Music saved!\n";
			break;
		}
		case '2':{
			list.DeQueue();
			break;
		}
		case '3':{
			list.PrintQueue();
			break;
		}
		default:{
			cout<<"Invalid Input!\n";
			break;
		}
	}
	switcher();
}

int main()
{
	cout<<"*********************************************\n";
	cout<<"*   qqqqqqq        q        q qqqqqqqq   q  *\n";
	cout<<"*  q       q       q        q q       q  q  *\n";
	cout<<"*  q   qq  q       q        q q       q  q  *\n";
	cout<<"*  q     q q       q        q qqqqqqqq   q  *\n";
	cout<<"*  q      qq        q      q  q             *\n";
	cout<<"*   qqqqqqqq         qqqqqq   q          q  *\n";
	cout<<"*           qqq                             *\n";
	cout<<"*********************************************\n";
	
	switcher();
}
